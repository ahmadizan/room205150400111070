package id.filkom.papb.room205150400111070_1;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ItemDAO {
    @Query("SELECT * FROM TableItem")
    List<TableItem> getAll();

    @Insert
    void insertAll(TableItem... items);

    @Delete
    void delete(TableItem item);

    @Update
    void update(TableItem item);
}
