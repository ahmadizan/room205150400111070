package id.filkom.papb.room205150400111070_1;

import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class AppExecutors {
    private static final Object LOCK = new Object();
    private static AppExecutors sInstace;
    private final Executor diskID;
    private final Executor mainThread;
    private final Executor networkID;

    public AppExecutors(Executor diskID, Executor mainThread, Executor networkID) {
        this.diskID = diskID;
        this.mainThread = mainThread;
        this.networkID = networkID;
    }

    public static AppExecutors getInstance(){
        if(sInstace == null){
            synchronized (LOCK){
                sInstace = new AppExecutors(Executors.newSingleThreadExecutor(),
                        Executors.newFixedThreadPool(3),new MainThreadExecutor());
            }
        }
        return sInstace;
    }

    public Executor diskID() { return diskID; }
    public Executor mainThread() { return mainThread; }
    public Executor networkID() { return networkID; }

    private static class MainThreadExecutor implements Executor{
        private Handler mainThreadHandler = new Handler(Looper.getMainLooper());

        @Override
        public void execute (@NonNull Runnable command){mainThreadHandler.post(command);}
    }
}
