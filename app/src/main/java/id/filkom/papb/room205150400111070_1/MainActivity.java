package id.filkom.papb.room205150400111070_1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    Button insert;
    Button view;
    EditText nim;
    EditText nama;
    TextView hasil;
    private appDatabase appDB;
    int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appDB = appDatabase.getInstance(getApplicationContext());
        insert = findViewById(R.id.insert);
        view = findViewById(R.id.view);
        nim = findViewById(R.id.nim);
        nama = findViewById(R.id.nama);
        hasil = findViewById(R.id.hasil);
        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i++;
                TableItem item = new TableItem();
                item.setId(i);
                item.setNim(nim.getText().toString());
                item.setNama(nama.getText().toString());
                AppExecutors.getInstance().diskID().execute(new Runnable() {
                    @Override
                    public void run (){
                        appDB.ItemDAO().insertAll(item);
                    }
                });
            }
        });
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppExecutors.getInstance().diskID().execute(new Runnable() {
                    @Override
                    public void run() {
                        List<TableItem> listItem = appDB.ItemDAO().getAll();
                        String s = " ";
                        for (TableItem item : listItem) {
                            s = item.getNim() + '\n' + item.getNama() + '\n';
                            }
                        hasil.setText(s);
                        }
                });
            }
        });
    }
}