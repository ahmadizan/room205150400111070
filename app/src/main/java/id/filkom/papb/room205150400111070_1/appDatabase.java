package id.filkom.papb.room205150400111070_1;

import android.content.Context;
import android.util.Log;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {TableItem.class}, version = 1)
public abstract class appDatabase extends RoomDatabase {
    private static final String DATABASE_NAME = "mahasiswa";
    private static appDatabase sInstance;
    private static final Object LOCK = new Object();
    private static final String LOG_TAG = appDatabase.class.getSimpleName();
    public static appDatabase getInstance(Context context){
        if(sInstance == null){
            synchronized (LOCK){
                Log.d(LOG_TAG,"Creating New Database Instance");
                sInstance = Room.databaseBuilder(context.getApplicationContext(),
                        appDatabase.class,appDatabase.DATABASE_NAME).build();
            }
        }
        Log.d(LOG_TAG,"Getting the database Instance");
        return sInstance;
    }

    public abstract ItemDAO ItemDAO();
}
